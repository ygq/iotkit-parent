# iotkit-parent

#### 介绍
此仓库为奇特物联(iotkit)物联网平台开源项目。
奇特物联是一个开源的物联网基础开发平台，提供了物联网及相关业务开发的常见基础功能, 能帮助你快速搭建自己的物联网相关业务平台。

系统包含了品类、物模型、消息转换、通讯组件（mqtt/EMQX通讯组件、小度音箱接入组件、onenet Studio接入组件）、云端低代码设备开发、设备管理、规则引擎、第三方平台接入、数据流转、数据可视化、报警中心等模块和智能家居APP（小程序），集成了[Sa-Token](https://gitee.com/dromara/sa-token) 认证框架。

 **前端项目见：** https://gitee.com/iotkit-open-source/iot-console-web

 **演示地址：** [演示地址](http://120.76.96.206)，账号：guest1,密码：guest123  (只读权限)

 **智能家居小程序：** https://gitee.com/iotkit-open-source/iot-mp-home ，微信搜索小程序： 奇特物联

 **小度接入：** 小度APP添加设备中搜索 奇特物联


 **系统截图** 
![输入图片说明](doc/screenshot.jpg)



#### 软件架构
软件架构说明
本系统采用springboot、mongodb、redis、elasticsearch、pulsar、sa-token等框架和第三方软件


#### 安装配置文档
见官网：
https://xiwasong.github.io



#### 技术文档

邀你加入「iot平台技术文档」知识库: https://ztktkv.yuque.com/g/ztktkv/gb3v6g/collaborator/join?token=zz5PUmXzGQqc4h9t# 
    
**这是我宝贵的技术文档分享，请给本仓库点个star  :star:  支持一下，谢谢！** 


#### 待办事项
- ->告警中心
- 数据大屏


#### 捐助与支持
如果您觉得我的开源软件对你有所帮助请关注、star、fork :kissing_heart: 。

如果我的开源软件应用到您的商业项目中，请务必通知到我，因为得到用户的认可是支撑开源的动力。

交流QQ群: 940575749 

微信群:

![输入图片说明](doc/ma.png)