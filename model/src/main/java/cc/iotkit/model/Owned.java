package cc.iotkit.model;

public interface Owned {

    String getId();

    String getUid();

    void setUid(String uid);

}
