package cc.iotkit.comp.biz;

import lombok.Data;

@Data
public class HttpConfig {

    private int port;

}
